import paramiko
import hashlib
from scp import SCPClient

def get_hash(filenam):
    filename = filenam
    sha256_hash = hashlib.sha256()
    with open(filename, "rb") as f:
        # Read and update hash string value in blocks of 4K
        for byte_block in iter(lambda: f.read(4096), b""):
            sha256_hash.update(byte_block)
        print(sha256_hash.hexdigest() + " " + filename)

        computed_hash = sha256_hash.hexdigest();
        return computed_hash

def ssh_scp_files(host, username,password):
    try:
        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh_client.connect(hostname=host, username =username, password =password)
        stdin, stdout, stderr = ssh_client.exec_command('cd $IIIDB/data;shasum -a256 b2mtab.gates; shasum -a256 post_map.gates')
        stdin, stdout, stderr = ssh_client.exec_command('cd $IIIDB/data;chmod 600 b2mtab.gates;rm -f b2mtab.gates')
        stdin, stdout, stderr = ssh_client.exec_command('cd $IIIDB/data;chmod 600 post_map.gates;rm -f post_map.gates')

       #postmap = '/mnt/c/users/ciara.brennan/PycharmProjects/MappingDrop/src/mappings/post_map.gates'
       # b2mtab = '/mnt/c/users/ciara.brennan/PycharmProjects/MappingDrop/src/mappings/b2mtab.gates'

        postmap = 'c:\\users\\ciara.brennan\\PycharmProjects\\MappingDrop\\src\\mappings\\post_map.gates'
        b2mtab = 'c:\\users\\ciara.brennan\\PycharmProjects\\MappingDrop\\src\\mappings\\b2mtab.gates'

        with SCPClient(ssh_client.get_transport()) as scp:
            scp.put(postmap, '/iiidb/data/post_map.gates')  # Copy my_file.txt to the server
        with SCPClient(ssh_client.get_transport()) as scp:
            scp.put(b2mtab, '/iiidb/data/b2mtab.gates')
        stdin, stdout, stderr = ssh_client.exec_command('cd $IIIDB/data;chmod 660 *.gates;ls -lah *.gates')
        #print(stderr.readlines())
        print(stdout.readlines())
        scp.close()
        ssh_client.close()

    except Exception as e:
        print('Connection Failed')
        print(e)


def check_files(host, username,password):
        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh_client.connect(hostname=host, username =username, password =password)
        stdin, stdout, stderr = ssh_client.exec_command('cd $IIIDB/data;sha256sum b2mtab.gates;sha256sum post_map.gates;ls -l *.gates')
        print(host + ' ***filecheck***')
        #print(stderr.readlines())
        print(stdout.readlines())
        ssh_client.close()



def check_file_by_name(host, username, password, file):
        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh_client.connect(hostname=host, username=username, password=password)
        command = 'cd $IIIDB/data;sha256sum ' + file
        stdin, stdout, stderr = ssh_client.exec_command(command)
        print(stdout.readlines())
        ssh_client.close()


if __name__ == '__main__':
    #print('precheck - compute sha256 of mapping files from DIS-18496')
    print('Local files: sha256sums')
    get_hash("./mappings/b2mtab.gates")
    get_hash("./mappings/post_map.gates")
    ssh_scp_files('devops-6916-app.iii-lab.eu', 'user', '*****')
    print('Remote Files: sha256sums')
    check_file_by_name('devops-6916-app.iii-lab.eu', 'user', 'password', 'b2mtab.gates')
    check_file_by_name('devops-6916-app.iii-lab.eu', 'user', 'password', 'post_map.gates')

